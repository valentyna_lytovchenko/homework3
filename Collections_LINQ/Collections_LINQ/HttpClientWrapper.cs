﻿using Collections_LINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ
{
    public class HttpClientWrapper
    {
        private readonly HttpClient HttpClient;

        private static HttpClientWrapper _instance = null;
        public static HttpClientWrapper Instance
        {
            get => _instance != null ? _instance : _instance = new HttpClientWrapper();
            private set => _instance = value;
        }

        private HttpClientWrapper() {
            HttpClient = new HttpClient(); 
        }

        private async Task<string> GetData(string URL)
        {
            string responseBody = "";
            try	
            {
                HttpResponseMessage response = await HttpClient.GetAsync(URL);
                response.EnsureSuccessStatusCode();
                responseBody = await response.Content.ReadAsStringAsync();
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");	
                Console.WriteLine("Message :{0} ",e.Message);
            }

            return responseBody;
        }

        public async Task<Dictionary<ProjectJSON, int>> Select1(int userId)
        {
            return JsonConvert.DeserializeObject<Dictionary<ProjectJSON, int>>(
                await GetData("https://localhost:44319/api/Linq/selectTask1"));
        }

        public async Task<ICollection<TaskJSON>> Select2(int userId)
        {
            return JsonConvert.DeserializeObject<ICollection<TaskJSON>>(
                await GetData("https://localhost:44319/api/Linq/selectTask2"));
        }

        public async Task<IEnumerable<(int, string)>> Select3(int userId)
        {
            return JsonConvert.DeserializeObject<IEnumerable<(int, string)>>(
                await GetData("https://localhost:44319/api/Linq/selectTask3"));
        }

        public async Task<IEnumerable<(int, string, List<UserJSON>)>> Select4()
        {
            return JsonConvert.DeserializeObject<IEnumerable<(int, string, List<UserJSON>)>>(
                await GetData("https://localhost:44319/api/Linq/selectTask4"));
        }

        public async Task<IEnumerable<(UserJSON, IEnumerable<TaskJSON>)>> Select5()
        {
            return JsonConvert.DeserializeObject<IEnumerable<(UserJSON, IEnumerable<TaskJSON>)>>(
                await GetData("https://localhost:44319/api/Linq/selectTask5"));
        }

        public async Task<(UserJSON, int, TaskJSON, ProjectJSON, int)> Select6(int userId)
        {
            return JsonConvert.DeserializeObject<(UserJSON, int, TaskJSON, ProjectJSON, int)>(
                await GetData("https://localhost:44319/api/Linq/selectTask6"));
        }

        public async Task<IEnumerable<(ProjectJSON, TaskJSON, TaskJSON, int)>> Select7()
        {
            return JsonConvert.DeserializeObject<IEnumerable<(ProjectJSON, TaskJSON, TaskJSON, int)>>(
                await GetData("https://localhost:44319/api/Linq/selectTask7"));
        }

        public async Task<List<ProjectJSON>> GetProjects()
        {
            return JsonConvert.DeserializeObject<List<ProjectJSON>>(
                await GetData("https://localhost:44319/api/Projects"));               
        }

        public async Task<List<TeamJSON>> GetTeams()
        {
            return JsonConvert.DeserializeObject<List<TeamJSON>>(
                await GetData("https://localhost:44319/api/Teams"));
        }

        public async Task<List<TaskJSON>> GetTasks()
        {
            return JsonConvert.DeserializeObject<List<TaskJSON>>(
                await GetData("https://localhost:44319/api/Tasks"));
        }

        public async Task<List<UserJSON>> GetUsers()
        {
            return JsonConvert.DeserializeObject<List<UserJSON>>(
                await GetData("https://localhost:44319/api/Users"));
        }

    }
}
