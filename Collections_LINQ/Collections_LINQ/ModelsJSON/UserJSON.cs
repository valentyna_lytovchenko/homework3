﻿using System;

namespace Collections_LINQ.Models
{
    public struct UserJSON
    {
        public int id;
        public int? teamId;
        public string firstName;
        public string lastName;
        public string email;
        public DateTime registeredAt;
        public DateTime birthDay; 
    }
}
