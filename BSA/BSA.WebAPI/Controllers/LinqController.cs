﻿using BSA.Common.DTO;
using Collections_LINQ;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : Controller
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("{selectTask1}")]
        public ActionResult<Dictionary<ProjectDTO, int>> SelectTasksNumberInProject(int userId)
        {
            return Ok(_linqService.SelectTasksNumberInProject(userId));
        }

        [HttpGet("{selectTask2}")]
        public ActionResult<ICollection<TaskDTO>> SelectTasks(int userId)
        {
            return Ok(_linqService.SelectTasks(userId));
        }

        [HttpGet("{selectTask3}")]
        public ActionResult<IEnumerable<(int, string)>> SelectTasksCompletedInCurrentYear(int userId)
        {
            return Ok(_linqService.SelectTasksCompletedInCurrentYear(userId));
        }

        [HttpGet("{selectTask4}")]
        public ActionResult<IEnumerable<(int, string, List<UserDTO>)>> SelectTeamsWithMembers()
        {
            return Ok(_linqService.SelectTeamsWithMembers());
        }

        [HttpGet("{selectTask5}")]
        public ActionResult<IEnumerable<(UserDTO, IEnumerable<TaskDTO>)>> SelectUsersWithTasks()
        {
            return Ok(_linqService.SelectUsersWithTasks());
        }

        [HttpGet("{selectTask6}")]
        public ActionResult<(UserDTO, int, TaskDTO, ProjectDTO, int)> SelectUserTasksInfo(int userId)
        {
            return Ok(_linqService.SelectUserTasksInfo(userId));
        }

        [HttpGet("{selectTask7}")]
        public ActionResult<IEnumerable<(ProjectDTO, TaskDTO, TaskDTO, int)>> GetProjectInfo()
        {
            return Ok(_linqService.GetProjectInfo());
        }
    }
}
