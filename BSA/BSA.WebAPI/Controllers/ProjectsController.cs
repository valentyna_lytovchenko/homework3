﻿using BSA.BLL.Services;
using BSA.Common.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetById(int id)
        {
            return Ok(_projectService.GetProjectById(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] ProjectDTO project)
        {
            _projectService.CreateProject(project);
            return Ok();
        }

        [HttpPut]
        public IActionResult Put([FromBody] ProjectDTO project)
        {
            _projectService.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}
