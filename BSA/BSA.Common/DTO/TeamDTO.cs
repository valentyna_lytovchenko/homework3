﻿using System;

namespace BSA.Common.DTO
{
    public class TeamDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }
    }
}
