﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Entities;
using System.Collections.Generic;

namespace BSA.BLL.Services
{
    public class TaskService : BaseService
    {
        public TaskService(IMapper mapper) : base(mapper) { }

        public IList<TaskDTO> GetTasks()
        {
            var tasks = _context.Tasks.GetList();
            return _mapper.Map<IList<TaskDTO>>(tasks);
        }

        public TaskDTO GetTaskById(int id)
        {
            var task = _context.Tasks.GetById(id);
            return _mapper.Map<TaskDTO>(task);
        }

        public void CreateTask(TaskDTO taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            _context.Tasks.Create(task);
        }

        public void DeleteTask(int id)
        {
            _context.Tasks.Delete(id);
        }

        public void UpdateTask(TaskDTO taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            _context.Tasks.Update(task);
        }
    }
}
