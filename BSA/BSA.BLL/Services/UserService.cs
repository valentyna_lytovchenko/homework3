﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using System.Collections.Generic;
using BSA.Common.DTO;
using BSA.DAL.Entities;

namespace BSA.BLL.Services
{
    public class UserService : BaseService
    {
        public UserService(IMapper mapper) : base(mapper) { }

        public IList<UserDTO> GetUsers()
        {
            var users =  _context.Users.GetList();
            return _mapper.Map<IList<UserDTO>>(users);
        }

        public UserDTO GetUserById(int id)
        {
            var user = _context.Users.GetById(id);
            return _mapper.Map<UserDTO>(user);
        }

        public void CreateUser(UserDTO userDto)
        {
            var user = _mapper.Map<User>(userDto);
            _context.Users.Create(user);
        }

        public void DeleteUser(int id)
        {
            _context.Users.Delete(id);
        }

        public void UpdateUser(UserDTO userDto)
        {
            var user = _mapper.Map<User>(userDto);
            _context.Users.Update(user);
        }
    }
}
