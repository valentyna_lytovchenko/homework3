﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Entities;
using System.Collections.Generic;

namespace BSA.BLL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IMapper mapper) : base(mapper) { }

        public IList<TeamDTO> GetTeams()
        {
            var teams = _context.Teams.GetList();
            return _mapper.Map<IList<TeamDTO>>(teams);
        }

        public TeamDTO GetTeamById(int id)
        {
            var team = _context.Teams.GetById(id);
            return _mapper.Map<TeamDTO>(team);
        }

        public void CreateTeam(TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            _context.Teams.Create(team);
        }

        public void DeleteTeam(int id)
        {
            _context.Teams.Delete(id);
        }

        public void UpdateTeam(TeamDTO teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            _context.Teams.Update(team);
        }
    }
}
