﻿using AutoMapper;
using BSA.DAL.UnitOfWork;

namespace BSA.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly UnitOfWork _context;
        private protected readonly IMapper _mapper;

        public BaseService(IMapper mapper)
        {
            _mapper = mapper;

            _context = UnitOfWork.Instance;
        }
    }
}
