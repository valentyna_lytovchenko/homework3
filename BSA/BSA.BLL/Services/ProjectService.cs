﻿using AutoMapper;
using BSA.BLL.Services.Abstract;
using BSA.Common.DTO;
using BSA.DAL.Entities;
using System.Collections.Generic;

namespace BSA.BLL.Services
{
    public class ProjectService : BaseService
    {
        public ProjectService(IMapper mapper) : base(mapper) { }

        public IList<ProjectDTO> GetProjects()
        {
            var projects = _context.Projects.GetList();
            return _mapper.Map<IList<ProjectDTO>>(projects);
        }

        public ProjectDTO GetProjectById(int id)
        {
            var project = _context.Projects.GetById(id);
            return _mapper.Map<ProjectDTO>(project);
        }

        public void CreateProject(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _context.Projects.Create(project);
        }

        public void DeleteProject(int id)
        {
            _context.Projects.Delete(id);
        }

        public void UpdateProject(ProjectDTO projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            _context.Projects.Update(project);
        }
    }
}
