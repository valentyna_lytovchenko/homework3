﻿using AutoMapper;
using BSA.Common.DTO;
using BSA.DAL.Entities;

namespace BSA.BLL.MapingProfiles
{
    public sealed class TaskProfile: Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>()
                .ForMember(tskDTO => tskDTO.state, scr => scr.MapFrom(tsk => tsk.state));

            CreateMap<TaskDTO, Task>()
                .ForMember(tsk => tsk.state, scr => scr.MapFrom(tskDTO => tskDTO.state))
                .ForMember(tsk => tsk.performer, scr => scr.Ignore());
        }
    }
}
