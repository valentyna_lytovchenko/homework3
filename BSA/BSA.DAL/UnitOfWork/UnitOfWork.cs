﻿using BSA.DAL.Repository;

namespace BSA.DAL.UnitOfWork
{
    public class UnitOfWork
    {
        private ProjectRepository projectRepository;
        private UserRepository userRepository;
        private TaskRepository taskRepository;
        private TeamRepository teamRepository;

        private static UnitOfWork _instance;
        public static UnitOfWork Instance { 
            get => _instance ??= new UnitOfWork();
        }

        private UnitOfWork() {}

        public ProjectRepository Projects
        {
            get
            {
                if (projectRepository == null)
                    projectRepository = new ProjectRepository();
                return projectRepository;
            }
        }

        public UserRepository Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository();
                return userRepository;
            }
        }

        public TaskRepository Tasks
        {
            get
            {
                if (taskRepository == null)
                    taskRepository = new TaskRepository();
                return taskRepository;
            }
        }

        public TeamRepository Teams
        {
            get
            {
                if (teamRepository == null)
                    teamRepository = new TeamRepository();
                return teamRepository;
            }
        }

    }
}
