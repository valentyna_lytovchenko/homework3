﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSA.DAL.Entities;

namespace BSA.DAL.Repository
{
    public class TaskRepository : IRepository<Task>
    {
        public List<Task> Tasks { get; private set; }

        public TaskRepository()
        {
            Tasks = new List<Task>();
        }

        public void Create(Task item)
        {
            Tasks.Add(item);
        }

        public void Delete(int id)
        {
            try
            {
                Tasks.RemoveAt(Tasks.FirstOrDefault(tsk => tsk.Id == id).Id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No tasks with this id.");
            }
        }

        public Task GetById(int id)
        {
            try
            {
                return Tasks.FirstOrDefault(tsk => tsk.Id == id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No tasks with this id.");
            }
        }

        public IEnumerable<Task> GetList()
        {
            return Tasks.AsReadOnly();
        }

        public void Update(Task item)
        {
            try
            {
                Tasks[Tasks.FirstOrDefault(tsk => tsk.Id == item.Id).Id] = item;
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No tasks with this id.");
            }
        }
    }
}
