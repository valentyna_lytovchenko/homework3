﻿using BSA.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.DAL.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        public List<Project> Projects { get; private set; }

        public ProjectRepository()
        {
            Projects = new List<Project>();
        }

        public void Create(Project item)
        {
            Projects.Add(item);
        }

        public void Delete(int id)
        {
            try
            {
                Projects.RemoveAt(Projects.FirstOrDefault(p => p.Id == id).Id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No projects with this id.");
            }            
        }

        public Project GetById(int id)
        {
            try
            {
                return Projects.FirstOrDefault(p => p.Id == id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No projects with this id.");
            }            
        }

        public IEnumerable<Project> GetList()
        {
            return Projects.AsReadOnly();            
        }

        public void Update(Project item)
        {
            try
            {
                Projects[Projects.FirstOrDefault(p => p.Id == item.Id).Id] = item;
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No projects with this id.");
            }            
        }
    }
}
