﻿using System;
using System.Collections.Generic;

namespace BSA.DAL.Repository
{
    interface IRepository<BaseEntity> 
    {
        IEnumerable<BaseEntity> GetList();
        BaseEntity GetById(int id);
        void Create(BaseEntity item);
        void Update(BaseEntity item);
        void Delete(int id);
    }
}
