﻿using BSA.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.DAL.Repository
{
    public class UserRepository : IRepository<User>
    {
        public List<User> Users { get; private set; }

        public UserRepository()
        {
            Users = new List<User>(); 
        }

        public void Create(User item)
        {
            Users.Add(item);
        }

        public void Delete(int id)
        {
            try
            {
                Users.RemoveAt(Users.FirstOrDefault(u => u.Id == id).Id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No users with this id.");
            }
        }

        public User GetById(int id)
        {
            try
            {
                return Users.FirstOrDefault(u => u.Id == id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No users with this id.");
            }
        }

        public IEnumerable<User> GetList()
        {
            return Users.AsReadOnly();
        }

        public void Update(User item)
        {
            try
            {
                Users[Users.FirstOrDefault(u => u.Id == item.Id).Id] = item;
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No users with this id.");
            }
        }
    }
}
