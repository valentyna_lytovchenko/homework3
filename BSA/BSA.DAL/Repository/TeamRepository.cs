﻿using BSA.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.DAL.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        public List<Team> Teams { get; private set; }

        public TeamRepository()
        {
            Teams = new List<Team>(); 
        }

        public void Create(Team item)
        {
            Teams.Add(item);
        }

        public void Delete(int id)
        {
            try
            {
                Teams.RemoveAt(Teams.FirstOrDefault(tm => tm.Id == id).Id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No teams with this id.");
            }
        }

        public Team GetById(int id)
        {
            try
            {
                return Teams.FirstOrDefault(tm => tm.Id == id);
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No teams with this id.");
            }
        }

        public IEnumerable<Team> GetList()
        {
            return Teams.AsReadOnly();
        }

        public void Update(Team item)
        {
            try
            {
                Teams[Teams.FirstOrDefault(tm => tm.Id == item.Id).Id] = item;
            }
            catch (Exception)
            {
                throw new IndexOutOfRangeException("No teams with this id.");
            }
        }
    }
}
