﻿using System;

namespace BSA.DAL.Entities
{
    public class Task: BaseEntity
    {
        public int projectId { get; set; }
        public User performer { get; set; } //
        public int performerId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int state { get; set; }
        public DateTime? finishedAt { get; private set; }
    }
}
