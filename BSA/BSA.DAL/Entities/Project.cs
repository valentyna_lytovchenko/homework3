﻿using System;
using System.Collections.Generic;

namespace BSA.DAL.Entities
{
    public class Project: BaseEntity
    {
        public User author { get; set; }
        public Team team { get; set; }
        public List<Task> tasks { get; set; }
        public int authorId { get; set; }
        public int teamId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime deadline { get; set; }
    }
}
