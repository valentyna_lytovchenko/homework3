﻿using System;

namespace BSA.DAL.Entities
{
    public class User : BaseEntity
    {
       public int? teamId { get; set; }
       public string firstName { get; set; }
       public string lastName { get; set; }
       public string email { get; set; }
       public DateTime registeredAt { get; private set; }
       public DateTime birthDay { get; set; }
    }
}

